import Head from 'next/head'
import About from '../components/About'
import Footer from '../components/Footer'
import Statistics from '../components/Statistics'
import RecentAddedBooks from '../components/RecentAddedBooks'
import PopularBooks from '../components/PopularBooks'
import ReservedBooks from '../components/ReservedBooks'
import PhotoGallery from '../components/PhotoGallery'
import ImageCarousel from '../components/ImageCarousel'
import WelcomeBox from '../components/WelcomeBox'

export default function Home() {
  return (
    <>
      <Head>
        <title>Library | Home</title>
      </Head>
      <div className="homePage">
        <ImageCarousel />
        <WelcomeBox />
        <About />
        <Statistics />
        <RecentAddedBooks />
        <PopularBooks />
        <ReservedBooks />
        <PhotoGallery />
        <Footer />
      </div>
    </>
  )
}
