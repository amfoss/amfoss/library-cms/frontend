module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["images.unsplash.com", "source.unsplash.com", "inkinmytea.files.wordpress.com", "images-na.ssl-images-amazon.com", "19en282jw7pc3zpwj22pg8v0-wpengine.netdna-ssl.com", "d1csarkz8obe9u.cloudfront.net", "i.gr-assets.com", "m.media-amazon.com"],
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
    imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
  }
}
