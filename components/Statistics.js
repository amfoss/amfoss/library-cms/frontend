import styles from '../styles/Statistics.module.css'
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import BookIcon from '@material-ui/icons/Book';

function Stats() {
    return (
        <div className={styles.stats}>
            <div className={styles.statsBlock}>
                <LibraryBooksIcon className={styles.statsIcon} style={{ fontSize: 80 }} />
                <p className={styles.statsTitle}>Total Books</p>
                <p className={styles.statsCount}>3254</p>
            </div>
            <div className={styles.statsBlock}>
                <LocalLibraryIcon className={styles.statsIcon} style={{ fontSize: 80 }} />
                <p className={styles.statsTitle}>Total Members</p>
                <p className={styles.statsCount}>254</p>
            </div>
            <div className={styles.statsBlock}>
                <BookIcon className={styles.statsIcon} style={{ fontSize: 80 }} />
                <p className={styles.statsTitle}>Reservations</p>
                <p className={styles.statsCount}>54</p>
            </div>
        </div>
    )
}

export default Stats

